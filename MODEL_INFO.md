
Model info for xresnet50
========================


Sequential (Input shape: 16 x 3 x 224 x 224)
============================================================================
Layer (type)         Output Shape         Param #    Trainable 
============================================================================
                     16 x 32 x 112 x 112 
Conv2d                                    864        True      
BatchNorm2d                               64         True      
ReLU                                                           
Conv2d                                    9216       True      
BatchNorm2d                               64         True      
ReLU                                                           
____________________________________________________________________________
                     16 x 64 x 112 x 112 
Conv2d                                    18432      True      
BatchNorm2d                               128        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 64 x 56 x 56   
MaxPool2d                                                      
Conv2d                                    4096       True      
BatchNorm2d                               128        True      
ReLU                                                           
Conv2d                                    36864      True      
BatchNorm2d                               128        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 256 x 56 x 56  
Conv2d                                    16384      True      
BatchNorm2d                               512        True      
Conv2d                                    16384      True      
BatchNorm2d                               512        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 64 x 56 x 56   
Conv2d                                    16384      True      
BatchNorm2d                               128        True      
ReLU                                                           
Conv2d                                    36864      True      
BatchNorm2d                               128        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 256 x 56 x 56  
Conv2d                                    16384      True      
BatchNorm2d                               512        True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 64 x 56 x 56   
Conv2d                                    16384      True      
BatchNorm2d                               128        True      
ReLU                                                           
Conv2d                                    36864      True      
BatchNorm2d                               128        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 256 x 56 x 56  
Conv2d                                    16384      True      
BatchNorm2d                               512        True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 128 x 56 x 56  
Conv2d                                    32768      True      
BatchNorm2d                               256        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 128 x 28 x 28  
Conv2d                                    147456     True      
BatchNorm2d                               256        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 512 x 28 x 28  
Conv2d                                    65536      True      
BatchNorm2d                               1024       True      
____________________________________________________________________________
                     16 x 256 x 28 x 28  
AvgPool2d                                                      
____________________________________________________________________________
                     16 x 512 x 28 x 28  
Conv2d                                    131072     True      
BatchNorm2d                               1024       True      
ReLU                                                           
____________________________________________________________________________
                     16 x 128 x 28 x 28  
Conv2d                                    65536      True      
BatchNorm2d                               256        True      
ReLU                                                           
Conv2d                                    147456     True      
BatchNorm2d                               256        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 512 x 28 x 28  
Conv2d                                    65536      True      
BatchNorm2d                               1024       True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 128 x 28 x 28  
Conv2d                                    65536      True      
BatchNorm2d                               256        True      
ReLU                                                           
Conv2d                                    147456     True      
BatchNorm2d                               256        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 512 x 28 x 28  
Conv2d                                    65536      True      
BatchNorm2d                               1024       True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 128 x 28 x 28  
Conv2d                                    65536      True      
BatchNorm2d                               256        True      
ReLU                                                           
Conv2d                                    147456     True      
BatchNorm2d                               256        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 512 x 28 x 28  
Conv2d                                    65536      True      
BatchNorm2d                               1024       True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 256 x 28 x 28  
Conv2d                                    131072     True      
BatchNorm2d                               512        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 256 x 14 x 14  
Conv2d                                    589824     True      
BatchNorm2d                               512        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 1024 x 14 x 14 
Conv2d                                    262144     True      
BatchNorm2d                               2048       True      
____________________________________________________________________________
                     16 x 512 x 14 x 14  
AvgPool2d                                                      
____________________________________________________________________________
                     16 x 1024 x 14 x 14 
Conv2d                                    524288     True      
BatchNorm2d                               2048       True      
ReLU                                                           
____________________________________________________________________________
                     16 x 256 x 14 x 14  
Conv2d                                    262144     True      
BatchNorm2d                               512        True      
ReLU                                                           
Conv2d                                    589824     True      
BatchNorm2d                               512        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 1024 x 14 x 14 
Conv2d                                    262144     True      
BatchNorm2d                               2048       True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 256 x 14 x 14  
Conv2d                                    262144     True      
BatchNorm2d                               512        True      
ReLU                                                           
Conv2d                                    589824     True      
BatchNorm2d                               512        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 1024 x 14 x 14 
Conv2d                                    262144     True      
BatchNorm2d                               2048       True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 256 x 14 x 14  
Conv2d                                    262144     True      
BatchNorm2d                               512        True      
ReLU                                                           
Conv2d                                    589824     True      
BatchNorm2d                               512        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 1024 x 14 x 14 
Conv2d                                    262144     True      
BatchNorm2d                               2048       True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 256 x 14 x 14  
Conv2d                                    262144     True      
BatchNorm2d                               512        True      
ReLU                                                           
Conv2d                                    589824     True      
BatchNorm2d                               512        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 1024 x 14 x 14 
Conv2d                                    262144     True      
BatchNorm2d                               2048       True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 256 x 14 x 14  
Conv2d                                    262144     True      
BatchNorm2d                               512        True      
ReLU                                                           
Conv2d                                    589824     True      
BatchNorm2d                               512        True      
ReLU                                                           
____________________________________________________________________________
                     16 x 1024 x 14 x 14 
Conv2d                                    262144     True      
BatchNorm2d                               2048       True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 512 x 14 x 14  
Conv2d                                    524288     True      
BatchNorm2d                               1024       True      
ReLU                                                           
____________________________________________________________________________
                     16 x 512 x 7 x 7    
Conv2d                                    2359296    True      
BatchNorm2d                               1024       True      
ReLU                                                           
____________________________________________________________________________
                     16 x 2048 x 7 x 7   
Conv2d                                    1048576    True      
BatchNorm2d                               4096       True      
____________________________________________________________________________
                     16 x 1024 x 7 x 7   
AvgPool2d                                                      
____________________________________________________________________________
                     16 x 2048 x 7 x 7   
Conv2d                                    2097152    True      
BatchNorm2d                               4096       True      
ReLU                                                           
____________________________________________________________________________
                     16 x 512 x 7 x 7    
Conv2d                                    1048576    True      
BatchNorm2d                               1024       True      
ReLU                                                           
Conv2d                                    2359296    True      
BatchNorm2d                               1024       True      
ReLU                                                           
____________________________________________________________________________
                     16 x 2048 x 7 x 7   
Conv2d                                    1048576    True      
BatchNorm2d                               4096       True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 512 x 7 x 7    
Conv2d                                    1048576    True      
BatchNorm2d                               1024       True      
ReLU                                                           
Conv2d                                    2359296    True      
BatchNorm2d                               1024       True      
ReLU                                                           
____________________________________________________________________________
                     16 x 2048 x 7 x 7   
Conv2d                                    1048576    True      
BatchNorm2d                               4096       True      
Sequential                                                     
ReLU                                                           
____________________________________________________________________________
                     16 x 2048 x 1 x 1   
AdaptiveAvgPool2d                                              
AdaptiveMaxPool2d                                              
____________________________________________________________________________
                     16 x 4096           
Flatten                                                        
BatchNorm1d                               8192       True      
Dropout                                                        
____________________________________________________________________________
                     16 x 512            
Linear                                    2097152    True      
ReLU                                                           
BatchNorm1d                               1024       True      
Dropout                                                        
____________________________________________________________________________
                     16 x 3              
Linear                                    1536       True      
____________________________________________________________________________

Total params: 25,635,168
Total trainable params: 25,635,168
Total non-trainable params: 0

Optimizer used: <function Adam at 0x799916fc5750>
Loss function: FlattenedLoss of CrossEntropyLoss()

Callbacks:
  - TrainEvalCallback
  - CastToTensor
  - Recorder
  - ProgressCallback